- 👋 Hi, I’m @mitexleo
- 👀 I’m interested in Programming, Privacy Activism and Blockchain Technology
- 🌱 I’m currently learning Python and Javascript 
- 💞️ I’m looking to collaborate on Open Source Projects
- 📫 How to reach me ...

<!---
mitexleo/mitexleo is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
